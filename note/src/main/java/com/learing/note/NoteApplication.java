package com.learing.note;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: https://blog.csdn.net/sinat_42483341/article/details/105173540
 * @author: 唐晓军
 * @date: 2021/11/1 13:14
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 */
@SpringBootApplication
public class NoteApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoteApplication.class, args);
    }

}
