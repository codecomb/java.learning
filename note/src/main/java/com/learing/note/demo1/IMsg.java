package com.learing.note.demo1;

/**
 * @description: TODO
 * @author: 唐晓军
 * @date: 2021/10/30 21:04
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
public interface IMsg {
    void handler(MessageInfo messageInfo);
}
