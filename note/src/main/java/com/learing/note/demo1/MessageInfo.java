package com.learing.note.demo1;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @description: TODO
 * @author: 唐晓军
 * @date: 2021/10/30 21:05
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
@Data
@AllArgsConstructor
public class MessageInfo {
    private String msgId;
    private String content;
}
