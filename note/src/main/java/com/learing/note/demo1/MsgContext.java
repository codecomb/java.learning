package com.learing.note.demo1;

import java.util.Map;

/**
 * @description: TODO
 * @author: 唐晓军
 * @date: 2021/10/30 21:08
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
public class MsgContext {
    public static IMsg getInstance(String msgId) {
        IMsg inter = null;
        Map<String, String> allClazz = MsgEnum.getAllClazz();
        String clazz = allClazz.get(msgId);
        if (msgId != null && msgId.trim().length() > 0) {
            try {
                try {
                    inter = (IMsg) Class.forName(clazz).newInstance();//调用无参构造器创建实例
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return inter;
    }
}
