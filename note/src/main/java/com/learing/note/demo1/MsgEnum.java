package com.learing.note.demo1;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: TODO
 * @author: 唐晓军
 * @date: 2021/10/30 21:08
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
public enum MsgEnum {
    MS066("MS003", "com.learing.note.demo1.file.MS003"),
    MS034("MS004", "com.learing.note.demo1.file.MS004"),
    MS064("MS064", "com.learing.note.demo1.file.MS064"),
    MS028("MS028", "com.learing.note.demo1.file.MS028"),
    MS003("MS003", "com.learing.note.demo1.file.MS003"),
    MS062("MS062", "com.learing.note.demo1.file.MS062"),
    MS154("MS154", "com.learing.note.demo1.file.MS154"),
    MS153("MS153", "com.learing.note.demo1.file.MS153"),
    MS033("MS033", "com.learing.note.demo1.file.MS033");
    private String msgid;
    private String clazz;

    public static Map<String, String> getAllClazz() {
        Map<String, String> map = new HashMap<String, String>();
        for (MsgEnum msgEnum : MsgEnum.values()) {
            map.put(msgEnum.getMsgid(), msgEnum.getClazz());
        }
        return map;
    }


    MsgEnum(String msgid, String clazz) {
        this.msgid = msgid;
        this.clazz = clazz;
    }


    public String getMsgid() {
        return msgid;
    }


    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }


    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }
}
