package com.learing.note.demo1.file;

import com.learing.note.demo1.IMsg;
import com.learing.note.demo1.MessageInfo;

/**
 * @description: TODO
 * @author: 唐晓军
 * @date: 2021/10/30 21:07
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
public class MS004 implements IMsg {
    @Override
    public void handler(MessageInfo messageInfo) {
        System.out.println("MS004");
    }
}
