package com.learing.note.studythread;

import org.springframework.cache.annotation.Cacheable;
import sun.java2d.cmm.ColorTransform;

import javax.naming.spi.NamingManager;
import javax.sound.midi.Soundbank;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @description: 创建线程方式一 继承Thread类
 * @author: 唐晓军
 * @date: 2021/10/30 8:33
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
public class Demo01 extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println("我在学习->" + i);
        }
    }

    public static void main(String[] args) {
        Demo01 demo01 = new Demo01();
        demo01.start();//并行执行
        demo01.run();//执行顺序不同，是先执行

        //main主线程
        for (int i = 0; i < 20; i++) {
            System.out.println("主线程->" + i);
        }
    }
}

class Demo_Join implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println("子线程->" + i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Demo_Join join = new Demo_Join();
        Thread thread = new Thread(join);
        thread.start();

        for (int i = 0; i < 20; i++) {
            if (i == 10) {
                thread.join();
            }
            System.out.println("主线程执行->" + i);
        }
    }
}

class God implements Runnable {
    @Override
    public void run() {
        while (true) {
            System.out.println("永远活着");
        }
    }
}

class You implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 36500; i++) {
            if (i == 36500) {
                System.out.println("dead");
            }
            System.out.println("你一生都开心的活着");
        }
    }
}

class Client01 {
    public static void main(String[] args) {
        God god = new God();
        You you = new You();

        Thread threadGod = new Thread(god);
        threadGod.setDaemon(true);
        threadGod.start();

        Thread threadYou = new Thread(you);
        threadYou.start();
    }
}


//方式一：继承Thread类
class MyThread1 extends Thread {
    @Override
    public void run() {
        System.out.println("MyThread1");
    }
}

//方式二：实现Runnable接口
class MyThread2 implements Runnable {
    @Override
    public void run() {
        System.out.println("MyThread2");
    }
}

//方式三：实现Callable接口
class MyThread3 implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        return 100;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        new MyThread1().start();

        new Thread(new MyThread2()).start();

        FutureTask<Integer> futureTask = new FutureTask<>(new MyThread3());
        new Thread(futureTask).start();
        Integer integer = futureTask.get();
        System.out.println(integer);
    }
}


