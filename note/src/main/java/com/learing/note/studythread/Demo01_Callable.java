package com.learing.note.studythread;

import com.sun.prism.shader.Solid_TextureFirstPassLCD_AlphaTest_Loader;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @description: TODO
 * @author: 唐晓军
 * @date: 2021/10/30 9:54
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
public class Demo01_Callable implements Callable<String> {
    @Override
    public String call() throws Exception {
        return "你好吗？";
    }

    public static void main(String[] args) {

    }
}

class Task implements Callable<String> {
    private int time;
    private String name;

    public Task(int time, String name) {
        this.time = time;
        this.name = name;
    }

    @Override
    public String call() throws Exception {
        TimeUnit.SECONDS.sleep(this.time);
        return name;
    }

    public static void main(String[] args) throws Exception {
//        ThreadPoolExecutor pool = new ThreadPoolExecutor(3, 3, 60, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10));
//        CompletionService<String> cs = new ExecutorCompletionService<>(pool);
//        cs.submit(new Task(3, "name" + 3));
//        cs.submit(new Task(1, "name" + 1));
//        cs.submit(new Task(2, "name" + 2));
//        for (int i = 0; i < 3; i++) {
//            System.out.println(cs.take().get());
//        }

//        BinaryOperator<Integer> operator = (x, y) -> x * y;
//        System.out.println(operator.apply(5, 7));


        List<String> testData = new ArrayList<String>();
        testData.add("张三");
        testData.add("李四");
        testData.add("王二");
        testData.add("麻子");

        StringBuilder identity = new StringBuilder();
        StringBuilder reduce = testData.stream()
                .flatMap(x -> Stream.of(x.split("")))
                .reduce(identity, (r, x) -> {
                    r.append(x);
                    return r;
                }, StringBuilder::append);
        System.out.println(identity == reduce); // true
        System.out.println(reduce.toString());//张三李四王二麻子

        ArrayList<String> identity2 = new ArrayList<>();
        ArrayList<String> result = testData.stream()
                .flatMap(x -> Stream.of(x.split("")))
                .reduce(identity2, (r, x) -> {
                    r.add(x);
                    return r;
                }, (r1, r2) -> {
                    r1.addAll(r2);
                    return r1;
                });
        System.out.println(identity2 == result);  // true
        System.out.println(result.toString()); //[张, 三, 李, 四, 王, 二, 麻, 子]
    }
}