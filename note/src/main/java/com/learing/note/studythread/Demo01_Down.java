package com.learing.note.studythread;

/**
 * @description: TODO
 * @author: 唐晓军
 * @date: 2021/10/30 9:46
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
public class Demo01_Down {

    public static void main(String[] args) {
        Down down1 = new Down("1", "2");
        Down down2 = new Down("2", "2");
        Down down3 = new Down("3", "3");
        down1.start();
        down2.start();
        down3.start();
    }
}

class WebDownloader {
    public void down(String url, String name) {
        System.out.println("正在下载文件 " + url);
    }
}

class Down extends Thread {

    private String url;
    private String name;

    public Down(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        WebDownloader webDownloader = new WebDownloader();
        webDownloader.down(url, name);
    }
}
