package com.learing.note.studythread;

/**
 * @description: 推荐使用 买火车票的例子
 * @author: 唐晓军
 * @date: 2021/10/30 9:50
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
public class Demo01_Runnable implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(Thread.currentThread().getName() + "我在学习->" + i);
        }
    }

    public static void main(String[] args) {
        Demo01_Runnable demo01_1 = new Demo01_Runnable();
        Demo01_Runnable demo01_2 = new Demo01_Runnable();
        Demo01_Runnable demo01_3 = new Demo01_Runnable();

        //创建线程对象
        new Thread(demo01_1, "1").start();
        new Thread(demo01_2, "2").start();
        new Thread(demo01_3, "3").start();

        for (int i = 0; i < 20; i++) {
            System.out.println("你好吗->" + i);
        }
    }
}


class TicketThread implements Runnable {

    private static int tickNums = 10;

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(Thread.currentThread().getName() + "--->买到了第" + i + "张票");
        }
    }

    public static void main(String[] args) {
        TicketThread ticket = new TicketThread();

        //多个线程操作同一个类
        new Thread(ticket, "张1").start();
        new Thread(ticket, "张2").start();
        new Thread(ticket, "张3").start();
    }
}