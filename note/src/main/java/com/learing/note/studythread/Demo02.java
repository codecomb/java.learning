package com.learing.note.studythread;

/**
 * @description: TODO
 * @author: 唐晓军
 * @date: 2021/10/30 8:53
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
public class Demo02 {

    static class Like2 implements ILike {
        @Override
        public void lambda() {
            System.out.println("第二种：静态内部类");
        }
    }

    public static void main(String[] args) {
        ILike like1 = new Like();
        like1.lambda();

        //第二种 静态内部类
        ILike like2 = new Like2();
        like2.lambda();

        class Like3 implements ILike {
            @Override
            public void lambda() {
                System.out.println("第3种：局部内部类");
            }
        }

        //第三种 局部内部类
        ILike like3 = new Like3();
        like3.lambda();

        //第四种 匿名内部类 没有类的名称，必须借助接口或父类
        ILike like4 = new ILike() {
            @Override
            public void lambda() {
                System.out.println("第4种：匿名内部类");
            }
        };
        like4.lambda();

        //第五种
        ILike like5 = () -> {
            System.out.println("第5种: lambda");
        };
        like5.lambda();
    }
}


interface ILike {
    void lambda();
}

class Like implements ILike {
    @Override
    public void lambda() {
        System.out.println("第一种调用方式");
    }
}
