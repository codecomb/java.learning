package com.learing.note.studythread;

/**
 * @description: TODO
 * @author: 唐晓军
 * @date: 2021/10/30 9:28
 * @version: 1.0.0
 * @company: 北京凤凰学易科技有限公司
 **/
public class Demo03 {

    public static void main(String[] args) {

        ILove love1 = (Integer a, Integer b) -> {
            System.out.println("说" + a + b);
        };
        love1.say(1, 2);


        love1 = (a, b) -> {
            System.out.println("说" + a + b);
        };
        love1.say(3, 4);
    }
}

interface ILove {
    void say(Integer a, Integer b);
}
